unit UGeoFunctions;

interface

// based on the article of Andy McGovern  on
// "Geographic Distance and Azimuth Calculations"
// here:
// http://www.codeguru.com/cpp/cpp/algorithms/article.php/c5115/Geographic-Distance-and-Azimuth-Calculations.htm

function EllipsoidDistance(var lat1, lon1, lat2, lon2: Double): Double; cdecl;

function GCDistance(var lat1, lon1, lat2, lon2: Double): Double; cdecl;

function ApproxDistance(var lat1, lon1, lat2, lon2: Double): Double; cdecl;

implementation

uses
  System.Math;

const
  DE2RA = 0.01745329252;
  ERAD = 6378.137;
  FLATTENING = 1.000000/298.257223563;// Earth flattening (WGS84)
  EPS = 0.000000000005;
  AVG_ERAD = 6371.0;

function EllipsoidDistance(var lat1, lon1, lat2, lon2: Double): Double;
var
  distance: Double;
  faz, baz: Double;
  r: Double;
  tu1, tu2, cu1, su1, cu2, x, sx, cx, sy, cy, y, sa, c2a, cz, e, c, d: Double;
  cosy1, cosy2: Double;
begin
  distance := 0.0;
  r := 1 - FLATTENING;

  if (lon1 = lon2) or (lat1 = lat2) then
  begin
    Result := distance;
    Exit;
  end;

  lon1 := lon1 * DE2RA;
  lon2 := lon2 * DE2RA;
  lat1 := lat1 * DE2RA;
  lat2 := lat2 * DE2RA;

  cosy1 := Cos(lat1);
	cosy2 := Cos(lat2);

  if (cosy1 = 0.0) then cosy1 := 0.0000000001;
	if (cosy2 = 0.0) then cosy2 := 0.0000000001;

  tu1 := r * Sin(lat1) / cosy1;
	tu2 := r * Sin(lat2) / cosy2;
	cu1 := 1.0 / Sqrt(tu1 * tu1 + 1.0);
	su1 := cu1 * tu1;
	cu2 := 1.0 / Sqrt(tu2 * tu2 + 1.0);
	x := lon2 - lon1;

	distance := cu1 * cu2;
	baz := distance * tu2;
	faz := baz * tu1;

  repeat
		sx := Sin(x);
		cx := Cos(x);
		tu1 := cu2 * sx;
		tu2 := baz - su1 * cu2 * cx;
		sy := Sqrt(tu1 * tu1 + tu2 * tu2);
		cy := distance * cx + faz;
		y := ArcTan2(sy, cy);
		sa := distance * sx / sy;
		c2a := -sa * sa + 1.0;
		cz := faz + faz;
		if (c2a > 0.0) then cz := -cz / c2a + cy;
		e := cz * cz * 2. - 1.0;
		c := ((-3.0 * c2a + 4.0) * FLATTENING + 4.0) * c2a * FLATTENING / 16.0;
		d := x;
		x := ((e * cy * c + cz) * sy * c + y) * sa;
		x := (1.0 - c) * x * FLATTENING + lon2 - lon1;
	until (Abs(d - x) <= EPS);

  x := Sqrt((1.0 / r / r - 1.0) * c2a + 1.0) + 1.0;
	x := (x - 2.0) / x;
	c := 1.0 - x;
	c := (x * x / 4.0 + 1.0) / c;
	d := (0.375 * x * x - 1.0) * x;
	x := e * cy;
	distance := 1.0 - e - e;
	distance := ((((sy * sy * 4.0 - 3.0) *
              distance * cz * d / 6.0 - x) * d / 4.0 + cz) * sy * d + y) *
              c * ERAD * r;

	Result := distance;
end;

function GCDistance(var lat1, lon1, lat2, lon2: Double): Double; cdecl;
var
  d: Double;
begin
  lon1 := lon1 * DE2RA;
  lon2 := lon2 * DE2RA;
  lat1 := lat1 * DE2RA;
  lat2 := lat2 * DE2RA;

  d := Sin(lat1)*Sin(lat2) + Cos(lat1)*Cos(lat2)*Cos(lon1 - lon2);

  Result := AVG_ERAD * ArcCos(d);
end;

function ApproxDistance(var lat1, lon1, lat2, lon2: Double): Double; cdecl;
var
 F, G, L, sing, cosl, cosf, sinl, sinf, cosg,
 S, C, W, R, H1, H2, D: Double;
begin
  lon1 := lon1 * DE2RA;
  lon2 := lon2 * DE2RA;
  lat1 := lat1 * DE2RA;
  lat2 := lat2 * DE2RA;

	F := (lat1 + lat2) / 2.0;
	G := (lat1 - lat2) / 2.0;
	L := (lon1 - lon2) / 2.0;

	sing := Sin(G);
	cosl := Cos(L);
	cosf := Cos(F);
	sinl := Sin(L);
	sinf := Sin(F);
	cosg := Cos(G);

	S := (sing * sing * cosl * cosl) + (cosf * cosf * sinl * sinl);
	C := (cosg * cosg * cosl * cosl) + (sinf * sinf * sinl * sinl);
	W := ArcTan2(Sqrt(S), Sqrt(C));
	R := Sqrt((S * C)) / W;
	H1 := (3 * R - 1.0) / (2.0 * C);
	H2 := (3 * R + 1.0) / (2.0 * S);
	D := 2 * W * ERAD;

	Result := (D * (1 + FLATTENING * H1 * sinf * sinf * cosg * cosg -
		        FLATTENING * H2 * cosf * cosf * sing * sing));
end;

end.
