Declare External Function EllipsoidDistance
  Double Precision, Double Precision, Double Precision, Double Precision
  Returns Double Precision By Value
  Entry_Point 'EllipsoidDistance' Module_Name 'GeoUDF.dll';

Declare External Function GCDistance
  Double Precision, Double Precision, Double Precision, Double Precision
  Returns Double Precision By Value
  Entry_Point 'GCDistance' Module_Name 'GeoUDF.dll';
  
Declare External Function ApproxDistance
  Double Precision, Double Precision, Double Precision, Double Precision
  Returns Double Precision By Value
  Entry_Point 'ApproxDistance' Module_Name 'GeoUDF.dll';

50 03 59N 5 42 53W
58 38 38N 3 04 12W
Distance: 968.9 km
http://www.movable-type.co.uk/scripts/latlong.html

50 03 59N = 50.06638888888889
5 42 53W  = -5.714722222222222

58 38 38N = 58.64388888885556
3 04 12W = -3.07

Select EllipsoidDistance(50.06638888888889, -5.714722222222222, 58.64388888885556, -3.07) EllipsoidDistance,
       GCDistance(50.06638888888889, -5.714722222222222, 58.64388888885556, -3.07) GCDistance,
       ApproxDistance(50.06638888888889, -5.714722222222222, 58.64388888885556, -3.07) ApproxDistance
From RDB$Database

EllipsoidDistance returns 969.9330 http://www.movable-type.co.uk/scripts/latlong-vincenty.html
GCDistance returns 968.8535 http://www.movable-type.co.uk/scripts/latlong.html
ApproxDistance returns 969.9399