unit UTestGeoFunctions;

interface

uses
  TestFramework;

type
  TTestGeoFunctions = class(TTestCase)
  private
    lat1, lon1, lat2, lon2: Double;
  protected
    procedure Setup; override;
  published
    procedure TestEllipsoidDistance;
    procedure TestGCDistance;
    procedure TestApproxDistance;

  end;

implementation


uses
  System.Math,
  System.Types,
  System.SysUtils,
  UGeoFunctions;

const
  cEpsilon = 0.0001;

{ TTestGeoFunctions }

procedure TTestGeoFunctions.Setup;
begin
  lat1 := 50.06638888888889;   // 50�  3' 59'' N
  lon1 := -5.714722222222222;  //  5� 42' 53'' W
  lat2 := 58.64388888885556;   // 58� 38' 38'' N
  lon2 := -3.07;               //  3�  4' 12'' W
end;

procedure TTestGeoFunctions.TestApproxDistance;
const
  cExpectedResult: Double = 969.9399;
var
  Result: Double;
begin
  Result := ApproxDistance(lat1, lon1, lat2, lon2);
  Check(CompareValue(cExpectedResult, Result, cEpsilon) = EqualsValue,
        Format('expected Result %8.4f is not equal %8.4f', [cExpectedResult, Result]));
end;

procedure TTestGeoFunctions.TestEllipsoidDistance;
const
  // http://www.movable-type.co.uk/scripts/latlong-vincenty.html
  cExpectedResult: Double = 969.9330;
var
  Result: Double;
begin
  Result := EllipsoidDistance(lat1, lon1, lat2, lon2);
  Check(CompareValue(cExpectedResult, Result, cEpsilon) = EqualsValue,
        Format('expected Result %8.4f is not equal %8.4f', [cExpectedResult, Result]));
end;

procedure TTestGeoFunctions.TestGCDistance;
const
  // http://www.movable-type.co.uk/scripts/latlong.html
  cExpectedResult: Double = 968.8535;
var
  Result: Double;
begin
  Result := GCDistance(lat1, lon1, lat2, lon2);
  Check(CompareValue(cExpectedResult, Result, cEpsilon) = EqualsValue,
        Format('expected Result %8.4f is not equal %8.4f', [cExpectedResult, Result]));
end;

initialization
  RegisterTest(TTestGeoFunctions.Suite);
end.
